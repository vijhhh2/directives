import { Directive, OnInit, Renderer2, ElementRef, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {
  // tslint:disable-next-line:no-inferrable-types
  @Input() defaultColor: string = 'transparent';
  // tslint:disable-next-line:no-inferrable-types
  @Input() highlightColor: string = 'green';
  // tslint:disable-next-line:no-inferrable-types
  @HostBinding('style.backgroundColor') backgroundColor: string = this.defaultColor;

  constructor(private elRef: ElementRef, private renderer: Renderer2) { }
  ngOnInit() {
  // this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'green');
  // this.renderer.setStyle(this.elRef.nativeElement, 'color', 'white');
  this.backgroundColor = this.defaultColor;
  }

  @HostListener('mouseover') mouseover() {
    // this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'green');
    this.renderer.setStyle(this.elRef.nativeElement, 'color', 'white');
    this.backgroundColor = this.highlightColor;
  }
  @HostListener('mouseleave') mouseleave() {
    // this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'transparent');
    this.renderer.setStyle(this.elRef.nativeElement, 'color', 'black');
    this.backgroundColor = this.defaultColor;
  }


}
